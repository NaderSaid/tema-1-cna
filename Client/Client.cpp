#include <iostream>
#include<HelloReply.grpc.pb.h>
#include<HelloRequest.grpc.pb.h>
#include<Message.grpc.pb.h>

#include<grpc/grpc.h>
#include<grpcpp/channel.h>
#include<grpcpp/create_channel.h>
#include<grpcpp/client_context.h>
#include<grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
    grpc_init();
    ClientContext context; 
    auto hello_stub = Greeter::NewStub(grpc::CreateChannel("localhost:8888",
        grpc::InsecureChannelCredentials()));
    std::cout << "Enter your name: ";
    std::string clientName;
    std::cin >> clientName;
    HelloRequest hello;
    hello.set_name(clientName);

    HelloReply response;
    auto status = hello_stub->SayHello(&context, hello, &response);

    if (status.ok())
    {
        std::cout << "Name delivered." << std::endl;
    }
    else
    {
        std::cout << "Failed!" << std::endl;
    }
    return 0;
}
