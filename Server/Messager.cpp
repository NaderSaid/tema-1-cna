
#include "Messager.h"

::grpc::Status Messager::SayHello(::grpc::ServerContext* context, const::HelloRequest* request, ::HelloReply* reply)
{
	std::string prefix("Hello ");
	reply->set_message(prefix + request->name());
	std::cout << reply->message() << "!" << std::endl;
	return ::grpc::Status::OK;
}
