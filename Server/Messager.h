#pragma once
#include <Message.grpc.pb.h>

class Messager final : public Greeter::Service
{
public:
	Messager() {};
	::grpc::Status SayHello(::grpc::ServerContext* context, const::HelloRequest* request, ::HelloReply* reply) override;
};

